@extends('pages.master')

@section('title')
   Users
@stop

@section('header')
    TASKS Users
@stop

@section ('data')
    <div>
        @foreach($users as $user)

            <h2><a href="{{route('showuser', [$user->id])}}">{{$user->firstname}} {{$user->lastname}}</a></h2>

        @endforeach
    </div>
<div class="links">
    <a href="https://laravel.com/docs">Documentation</a>
    <a href="https://laracasts.com">Laracasts</a>
    <a href="/post/1">Our Job Profile</a>
    <a href="/profile">Company Profile</a>
    <a href="/contact">Contact Us</a>
    <a href="/about">about</a>
</div>
    @stop

@section('footer')
    <hr>
    Thank you for using TASKS.
    @stop