@extends('pages.master')

@section('title')
    TASKS is about a better tommorow not today
@stop

@section('header')
    About Us
@stop

@section('data')

    <div class ="dark">
    <figure>
        <img class="about" src="/images/elon.jpg" height="300" width="450" alt="elon">
        <figcaption> Elon wants you to go to mars.</figcaption>
    </figure>
    <p style="text-align:left; padding-left:20%; padding-right:20%; padding-top:10px">
        We want you to live a fulfilling life. Help humanity carry the first light to
        mars. Living a dangerous, exciting and a life where risk of fatality is high.
        Are you prepare to die and pave the way for the future of humanity?
    </p>
</div>
@stop