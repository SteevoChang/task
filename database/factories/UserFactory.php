<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return ['firstname' => $faker->firstname,
           'lastname'=> $faker->lastname,
            'email' => $faker->unique()->email,
            'linkedinurl' => $faker->url,
            'streetaddress' => $faker->streetAddress,
            'city' => $faker->numberBetween(1, 250000),
            'stateid' => $faker->numberBetween(1, 5000),
            'countryid' => $faker->numberBetween(1, 200),
            'postalzip' => $faker->postcode,
            'workphone' => $faker->unique()->phoneNumber,
            'workphoneextension' => $faker->numberBetween(1, 1000),
            'mobilephone' => $faker->unique()->phoneNumber,
            'homephone' => $faker->unique()->phoneNumber,
         //   'password' => $faker->firstname,
        'middlename' => $faker->unique()->email,
        'password' => $password ?: $password = bcrypt('$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm'), // secret
        'remember_token' => str_random(10),
    ];
});
