<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 4/22/2018
 * Time: 2:09 PM
 */

namespace database\seeds;
use Illuminate\Database\Seeder;
class UsersTableSeeder extends \DatabaseSeeder
{
    public function run()
    {
        factory('App\User', 100)->create();
    }
}