<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 4/21/2018
 * Time: 5:59 PM
 */

namespace App\Http\Controllers;

class PagesController extends Controller
{

    public function contact () {
        return view ('pages.contactus');
    }

    public function about() {
        return view('pages.aboutus');
    }
}